<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>File Management</title>
    <!-- Add Bootstrap CSS link here -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <!-- Navigation Bar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">File Management</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <span class="nav-link">Welcome, User</span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <!-- Sidebar -->
            <nav id="sidebar" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
                <!-- Sidebar content -->
            </nav>

            <!-- Main content -->
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <!-- Page content goes here -->
                <h1>Financial Management</h1>
                
                <!-- Add Operation: -->
                <div class="container">
                    <h2>Add Financial Record</h2>
                    <form id="addFinancialForm">
                        <div class="form-group">
                            <label for="que_name">Financial Name</label>
                            <input type="text" class="form-control" id="que_name" name="que_name" required>
                        </div>
                        <!-- Add other input fields for file attachment and other data as needed -->
                        <button type="submit" class="btn btn-primary">Add Record</button>
                    </form>
                </div>

                <!-- List Operation: -->
                <div class="container">
                    <h2>Financial Records List</h2>
                    <ul id="financialList"></ul>
                </div>

                <!-- Delete Operation: -->
                <div class="container">
                    <h2>Delete Financial Record</h2>
                    <form id="deleteFinancialForm">
                        <div class="form-group">
                            <label for="recordId">Record ID</label>
                            <input type="text" class="form-control" id="recordId" name="recordId" required>
                        </div>
                        <button type="submit" class="btn btn-danger">Delete Record</button>
                    </form>
                </div>

                <!-- Update Operation: -->
                <div class="container">
                    <h2>Update Financial Record</h2>
                    <form id="updateFinancialForm">
                        <div class="form-group">
                            <label for="updateId">Record ID</label>
                            <input type="text" class="form-control" id="updateId" name="updateId" required>
                        </div>
                        <!-- Add other input fields for updating data as needed -->
                        <button type="submit" class="btn btn-warning">Update Record</button>
                    </form>
                </div>

                <!-- JavaScript code for handling API requests (Add, List, Delete, Update) -->
                <script>
                    // JavaScript code for handling API requests
                    // ...
                </script>
            </main>
        </div>
    </div>

    <!-- Add Bootstrap JS and jQuery scripts here -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
