<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/** CUSTOM CONSTANTS */
define('STATIC_PASSWORD', '123456');
define('PER_PAGE', 10);



define('AMAZONS3_BUCKET', 'jansuraaj.org');
defined("AWS_ACCESSKEY") or define('AWS_ACCESSKEY', 'AKIA6MPW3FXCJ272UYHK');
defined('AWS_SECRET_KEY') or define('AWS_SECRET_KEY', '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');
defined('PATH_S3') or define('PATH_S3', 'https://s3.ap-south-1.amazonaws.com/jansuraaj.org/');


define('HOMEPAGE_VIDEO_GALLARY_THUMB_PATH', 'testing/js-assets/gallery-1/videos/');
define('HOMEPAGE_IMAGE_GALLARY_PATH', 'testing/js-assets/gallery-1/images/');

define('HOMEPAGE_TESTIMONIAL_THUMB_PATH', 'testing/js-assets/testimonials/');

define('HOMEPAGE_NEWS_ARTICLE_ENG_THUMB_PATH', 'testing/js-assets/images-1/media/en/');
define('HOMEPAGE_NEWS_ARTICLE_HIN_THUMB_PATH', 'testing/js-assets/images-1/media/hi/');

define('HOMEPAGE_PRESS_CONFERENCE_ENG_THUMB_PATH', 'testing/js-assets/gallery-1/conferences/en/');
define('HOMEPAGE_PRESS_CONFERENCE_HIN_THUMB_PATH', 'testing/js-assets/gallery-1/conferences/hi/');

define('HOMEPAGE_PILLARS_OF_JANSURAAJ_ENG_THUMB_PATH', 'testing/js-assets/3-pillers/');
define('HOMEPAGE_PILLARS_OF_JANSURAAJ_HIN_THUMB_PATH', 'testing/js-assets/3-pillers/');
define('HOMEPAGE_PILLARS_OF_JANSURAAJ_ICON_PATH', 'testing/js-assets/3-pillers/');


define('HOMEPAGE_UPCOMINGYATRA_THUMB_PATH', 'testing/js-assets/padayatratimetable/');

define('HOMEPAGE_SLIDER_THUMB_PATH', 'testing/js-assets/images-1/');

define('HOMEPAGE_VISION_ENG_THUMB_PATH', 'testing/js-assets/images-2/en/');
define('HOMEPAGE_VISION_HIN_THUMB_PATH', 'testing/js-assets/images-2/hi/');

define('HOMEPAGE_BANNER_ENG_THUMB_PATH', 'testing/js-assets/images-1/en/video/');
define('HOMEPAGE_BANNER_HIN_THUMB_PATH', 'testing/js-assets/images-1/hi/video/');
define('HOMEPAGE_BANNER_VIDEO_THUMB_PATH', 'testing/js-assets/banner_videos/');

define('MEDIA_PATH', 'testing/media/');


/***  START API RESPONSE MESSAGES **** */

define('SESSION_EXPIRED','Your session has been expired');
define('TRY_AGAIN','Something went wrong. Please try again');
define('RECORD_NOT_EXIST','Record does not exist');
define('RECORD_ADDED','Record has been added successfully');
define('RECORD_FETCHED','Record has been fetched successfully');
define('RECORD_UPDATED','Record has been updated successfully');
define('STATUS_UPDATED','Status has been updated successfully');
define('INVALID_ID','Invalid or missing id');
define('INVALID_METHOD','Invalid request method');


// define('FILEINFO_MIME_TYPE','application/pdf');


/***  END API RESPONSE MESSAGES **** */
