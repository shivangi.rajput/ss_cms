<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


/**** User Management APIs *****/

$route['api/login']                     = 'api/User/login';
$route['api/insert_financial']          = 'api/Financial/insert_financial';
$route['api/get_financial_list']        = 'api/Financial/get_financial_list';
$route['api/delete_financial_detail']   = 'api/Financial/delete_financial_detail';
$route['api/update_financial_detail']   = 'api/Financial/update_financial_detail';

$route['api/insert_AR']                 = 'api/AnnualReport/insert_AR';
$route['api/get_AR_list']               = 'api/AnnualReport/get_AR_list';
$route['api/delete_AR_detail']          = 'api/AnnualReport/delete_AR_detail';
$route['api/update_AR_detail']          = 'api/AnnualReport/update_AR_detail';

$route['api/insert_IP']                 = 'api/InvestorPresentation/insert_IP';
$route['api/get_IP_list']               = 'api/InvestorPresentation/get_IP_list';
$route['api/delete_IP_detail']          = 'api/InvestorPresentation/delete_IP_detail';
$route['api/update_IP_detail']          = 'api/InvestorPresentation/update_IP_detail';

$route['api/insert_SEF']                = 'api/StockExchangeFillings/insert_SEF';
$route['api/get_SEF_list']              = 'api/StockExchangeFillings/get_SEF_list';
$route['api/delete_SEF_detail']         = 'api/StockExchangeFillings/delete_SEF_detail';
$route['api/update_SEF_detail']         = 'api/StockExchangeFillings/update_SEF_detail';

$route['api/insert_financial']          = 'api/Financial/insert_financial';
$route['api/get_financial_list']        = 'api/Financial/get_financial_list';
$route['api/delete_financial_detail']   = 'api/Financial/delete_financial_detail';
$route['api/update_financial_detail']                 = 'api/Financial/update_financial_detail';

$route['api/insert_financial']                 = 'api/Financial/insert_financial';
$route['api/get_financial_list']                 = 'api/Financial/get_financial_list';
$route['api/delete_financial_detail']                 = 'api/Financial/delete_financial_detail';
$route['api/update_financial_detail']                 = 'api/Financial/update_financial_detail';

$route['api/insert_financial']                 = 'api/Financial/insert_financial';
$route['api/get_financial_list']                 = 'api/Financial/get_financial_list';
$route['api/delete_financial_detail']                 = 'api/Financial/delete_financial_detail';
$route['api/update_financial_detail']                 = 'api/Financial/update_financial_detail';