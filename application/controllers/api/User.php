<?php

require APPPATH . 'libraries/REST_Controller.php';

class User extends REST_Controller
{

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}
		parent::__construct();
		$this->load->database();
		$this->load->library(array("form_validation"));
		$this->load->helper("security");
		$this->load->library('email');
		date_default_timezone_set('Asia/Kolkata');
	}


	// Login
	public function login_post()
	{

		if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $post_data = $this->input->post();
            log_message('debug', 'POST Data: ' . json_encode($post_data));

			$email       = trim($this->security->xss_clean($this->input->post("email")));
			$password_text 	= trim($this->security->xss_clean($this->input->post("password")));

			// form validation for inputs
            
			$this->form_validation->set_rules("email", "Email", "required|valid_email");
			$this->form_validation->set_rules("password", "Password", "required");

			//checking form submittion have any error or not
			if ($this->form_validation->run() === FALSE) {
				$err = $this->form_validation->error_array();
				$arr = array_values($err);
				// we have some errors
				$this->response(array(
					"valid" => true,
					"status" => 'NOK',
					"result" => array(
						"message" => $arr[0]
					)
				), REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
			} else {
				$whereUser 		= array('where' => array('email' => $email, 'password' => $password_text));
				$tableName 		= 'user';
				$getUserDetails = $this->common_model->fetch_data($tableName, '*', $whereUser, true);
				if (empty($getUserDetails)) {
					$this->response(
						array(
							"valid"	 => true,
							"status" => 'NOK',
							"result" => array(
								"message" => "Invalid Email or Password"
							)
						),
						REST_Controller::HTTP_UNPROCESSABLE_ENTITY
					);
				} else {
					$user_id 	   = $getUserDetails['id'];
					$currentTime   = date("Y-m-d H:i:s");
					$generatetoken = $this->common_model->getToken(60);
					$data = array(
						'user_id' => $user_id,
						'token'   => $generatetoken,
						'submitted_timesamp' => $currentTime,
					);
					$whereUser = array('where' => array('user_id' => $user_id));
					$tableName = 'users_token';
					$getToken  = $this->common_model->fetch_data($tableName, '*', $whereUser, true);
					if (empty($getToken)) {
						$this->common_model->insert_single($tableName, $data);
					} else {
						$this->common_model->update_single($tableName, $data, $whereUser);
					}

					$secret_key      		= $this->common_model->secret_key;
				
					$info['name'] 			= $getUserDetails['name'];
					$info['email'] 			= $getUserDetails['email'];
					$info['uid'] 	= $getUserDetails['id'];



					$info['token'] 			= $generatetoken;
					$this->response(array(
						"valid"  => true,
						"status" => 'OK',
						"result" => array(
							"data" => $info,
							"message" => "Login Successfully"
						)
					), REST_Controller::HTTP_OK);
				}
			}
		}
	}
}
