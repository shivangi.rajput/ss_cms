<?php

require APPPATH . 'libraries/REST_Controller.php';

class Financial extends REST_Controller
{

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        $this->load->helper('s3');
        $this->load->database();
        $this->load->model(array("common_model"));
        $this->load->helper("security");
        
        date_default_timezone_set('Asia/Kolkata');
    }

    // Insert 

    public function insert_financial_post()
{
    if ($this->input->server('REQUEST_METHOD') == 'POST') {
        if (!empty($this->userData)) {
            $login_user_id  = $this->userData['id'];
            $financial_name = $this->input->post('que_name');

            $data = array();
            
            if (isset($_FILES['document_attachment']['tmp_name']) && is_uploaded_file($_FILES['document_attachment']['tmp_name'])) {
                
                $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
                $media_type = $this->common_model->getFiletype($_FILES["document_attachment"]['type']);
                $mimeType = finfo_file($fileInfo, $_FILES['document_attachment']['tmp_name']);
                $print_r($_FILES['document_attachment']); die();
                // Check if the MIME type indicates a PDF
                if ($mimeType === 'application/pdf') {
                    if ($fileInfo['filename']) {
                        $fileExt = $fileInfo['extension'];
                        $fileName = $fileInfo['filename'];
                        $fileName = $this->common_model->slugify($fileName);
                        $uniqueFileName = $fileName . '_' . strtotime("now") . '.' . $fileExt;
                        
                        // Read file contents
                        $fileData = file_get_contents($_FILES['document_attachment']['tmp_name']);
                        
                        // Upload file to S3
                        $uploadRes = upload_file_s3($fileData, 'pdf', $uniqueFileName);
                        
                        print_r($updateRes); die();
                        $uploadedImage = $uniqueFileName;
                    }
                   
    // ...
    
                    $data['document_attachment']       = $uploadRes;
                    $data['que_name']                   = $financial_name;
                }
                // print_r($data); die();
                    $insert_data = $this->common_model->insert_single('financial', $data);
    
                        if (!empty($insert_data)) {
                            $this->response(array(
                                "valid" => true,
                                "status" => 'OK',
                                "result" => array(
                                    "message" => "Record added successfully"
                                )
                            ), REST_Controller::HTTP_OK);
                        } else {
                            // Handle database insert error
                            $this->response(array(
                                "valid" => false,
                                "status" => 'NOK',
                                "result" => array(
                                    "message" => "Failed to add record"
                                )
                            ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                        }
                    // It's a PDF file, you can proceed with the upload
                    // ...
                } else {
                    // Invalid file type
                    $this->response(array(
                        "valid" => false,
                        "status" => 'NOK',
                        "result" => array(
                            "message" => "Invalid file type. Only PDF files are allowed."
                        )
                    ), REST_Controller::HTTP_BAD_REQUEST);
                }
            
                // finfo_close($fileInfo);
            }             

        } else {
            $this->response(
                array(
                    "valid" => false,
                    "status" => 'NOK',
                    "result" => array(
                        "message" => "Session expired."
                    )
                ),
                REST_Controller::HTTP_UNAUTHORIZED
            );
        }
    
}


    /* Get List */
    public function get_financial_list_post()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if (!empty($this->userData)) {
                $table_name = 'financial';
                $fields = '*';
                $data = $this->common_model->fetch_data($table_name, $fields);
    
                if (!empty($data)) {
                    // Modify the data to include the PDF content as a base64-encoded string
                    foreach ($data as &$record) {
                        $pdfData = $record['document_attatchment'];
                        $pdfBase64 = base64_encode($pdfData);
                        $record['document_attatchment'] = $pdfBase64;
                    }
    
                    $this->response(array(
                        "valid" => true,
                        "status" => 'OK',
                        "result" => array(
                            "message" => "Data fetched successfully",
                            "data" => $data
                        )
                    ), REST_Controller::HTTP_OK);
                } else {
                    $this->response(
                        array(
                            "valid" => false,
                            "status" => 'NOK',
                            "result" => array(
                                "message" => "No financial records exist."
                            )
                        ),
                        REST_Controller::HTTP_NOT_FOUND
                    );
                }
            } else {
                $this->response(
                    array(
                        "valid" => false,
                        "status" => 'NOK',
                        "result" => array(
                            "message" => "Session expired."
                        )
                    ),
                    REST_Controller::HTTP_UNAUTHORIZED
                );
            }
        }
    }
    
    
    public function download_pdf($pdfBase64)
    {
        // Decode the base64-encoded PDF data
        $pdfData = base64_decode($pdfBase64);
    
        // Set the appropriate headers for PDF
        header('Content-Type: application/pdf');
        header('Content-Disposition: inline; filename="document.pdf"'); // Change the filename as needed
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
    
        // Output the PDF content to the browser
        echo $pdfData;
    }


    /* Delete */

    public function delete_financial_detail_post()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if (!empty($this->userData)) {
                $login_user_id = $this->userData['id'];
                $id            = $this->input->post('id');

                    $table_name   = 'financial';
                    $updateStatus = array(
                        'status'             => '1',
                        'updated_at'  => date("Y-m-d H:i:s"),
                        'updated_by'         => $login_user_id
                    );

                    $whereCond = array(
                        'id' => $id
                    );
                    $updateRes = $this->common_model->updateRecord($updateStatus, $whereCond, $table_name);

                    if (!empty($updateRes)) {
                        $this->response(array(
                            "valid" => true,
                            "status" => 'OK',
                            "result" => array(
                                "message" => "Data deleted successfully"
                            )
                        ), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array(
                            "valid"   => false,
                            "status"  => 'NOK',
                            "result"  => array(
                                "message" => "ID is invalid "
                            )
                        ), REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response(array(
                        "valid"   => false,
                        "status"  => 'NOK',
                        "result"  => array(
                            "message" => "Session expired."
                        )
                    ), REST_Controller::HTTP_OK);
                }
            }
    }

    /* Update */

    public function update_financial_detail_post()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if (!empty($this->userData)) {
                $login_user_id     = $this->userData['id'];
                $id                = $this->input->post('id');
                $financial_name    = $this->input->post('que_name');
                $document          = $this->input->post('document_attatchment');
    
                

                    $table_name   = 'financial';
                    $updateStatus = array(
                        'que_name'             => $financial_name,
                        'document_attatchment' => $document,
                        'updated_at'  => date("Y-m-d H:i:s"),
                        'updated_by'         => $login_user_id
                    );

                    $whereCond = array(
                        'id' => $id
                    );
                    $updateRes = $this->common_model->updateRecord($updateStatus, $whereCond, $table_name);

                    if (!empty($updateRes)) {
                        $this->response(array(
                            "valid" => true,
                            "status" => 'OK',
                            "result" => array(
                                "message" => "Data Updated Successfully"
                            )
                        ), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array(
                            "valid"   => false,
                            "status"  => 'NOK',
                            "result"  => array(
                                "message" => RECORD_NOT_EXIST
                            )
                        ), REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response(array(
                        "valid"   => false,
                        "status"  => 'NOK',
                        "result"  => array(
                            "message" => INVALID_METHOD
                        )
                    ), REST_Controller::HTTP_OK);
                }
            }
    }

}
