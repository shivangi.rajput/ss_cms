<?php

require APPPATH . 'libraries/REST_Controller.php';

class CreditRatings extends REST_Controller
{

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        $this->load->database();
        $this->load->model(array("common_model"));
        $this->load->helper("security");
        date_default_timezone_set('Asia/Kolkata');
    }

    // Insert 

    public function insert_CR_post()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if (!empty($this->userData)) {
                $login_user_id  = $this->userData['id'];
                $rating_agency   = $this->input->post('rating_agency');
                $rated_instruments   = $this->input->post('rated_instruments');
                $credit_rating   = $this->input->post('credit_rating');

                $data = array();
                $data['rating_agency']        = $rating_agency;
                $data['rated_instruments']               = $rated_instruments;
                $data['credit_rating']               = $credit_rating;
                $data['created_at']     = date("Y-m-d H:i:s");
                $data['created_by']            = $login_user_id;


                $insert_data = $this->common_model->insert_single('credit_ratings', $data);
            
                if (!empty($insert_data)) {
                    $this->response(array(
                        "valid" => true,
                        "status" => 'OK',
                        "result" => array(
                            "message" => RECORD_ADDED
                        )
                    ), REST_Controller::HTTP_OK);
                }
            } else {
                $this->response(
                    array(
                        "valid" => false,
                        "status" => 'NOK',
                        "result" => array(
                            "message" => SESSION_EXPIRED
                        )
                    ),
                    REST_Controller::HTTP_UNAUTHORIZED
                );
            }
        }
    }

    /* Get List */
    public function get_CR_list_post()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if (!empty($this->userData)) {
                $table_name                  = 'credit_ratings';
                $fields                      =  '*';
                $data                        = $this->common_model->fetch_data($table_name, $fields);

                if (!empty($data)) {
                    $this->response(array(
                        "valid" => true,
                        "status" => 'OK',
                        "result" => array(
                            "message" =>  "Data fetched successfully",
                            "data"    =>  $data
                        )
                    ), REST_Controller::HTTP_OK);
                } else {
                    $this->response(
                        array(
                            "valid" => false,
                            "status" => 'NOK',
                            "result" => array(
                                "message" => RECORD_NOT_EXIST
                            )
                        ),
                        REST_Controller::HTTP_UNAUTHORIZED
                    );
                }
            } else {
                $this->response(
                    array(
                        "valid" => false,
                        "status" => 'NOK',
                        "result" => array(
                            "message" => SESSION_EXPIRED
                        )
                    ),
                    REST_Controller::HTTP_UNAUTHORIZED
                );
            }
        }
    }

    /* Delete */

    public function delete_CR_detail_post()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if (!empty($this->userData)) {
                $login_user_id = $this->userData['id'];
                $id            = $this->input->post('id');

                    $table_name   = 'credit_ratings';
                    $updateStatus = array(
                        'status'             => '1',
                        'updated_at'  => date("Y-m-d H:i:s"),
                        'updated_by'         => $login_user_id
                    );

                    $whereCond = array(
                        'id' => $id
                    );
                    $updateRes = $this->common_model->updateRecord($updateStatus, $whereCond, $table_name);

                    if (!empty($updateRes)) {
                        $this->response(array(
                            "valid" => true,
                            "status" => 'OK',
                            "result" => array(
                                "message" => "Data deleted successfully"
                            )
                        ), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array(
                            "valid"   => false,
                            "status"  => 'NOK',
                            "result"  => array(
                                "message" => RECORD_NOT_EXIST
                            )
                        ), REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response(array(
                        "valid"   => false,
                        "status"  => 'NOK',
                        "result"  => array(
                            "message" => INVALID_METHOD
                        )
                    ), REST_Controller::HTTP_OK);
                }
            }
    }

    /* Update */

    public function update_CR_detail_post()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            if (!empty($this->userData)) {
                $login_user_id     = $this->userData['id'];
                $id                = $this->input->post('id');
                $rating_agency    = $this->input->post('rating_agency');
                $rated_instruments          = $this->input->post('rated_instruments');
                $credit_rating          = $this->input->post('credit_rating');
    
                

                    $table_name   = 'credit_ratings';
                    $updateStatus = array(
                        'rating_agency'             => $rating_agency,
                        'rated_instruments' => $rated_instruments,
                        'credit_rating' => $credit_rating,
                        'updated_at'  => date("Y-m-d H:i:s"),
                        'updated_by'         => $login_user_id
                    );

                    $whereCond = array(
                        'id' => $id
                    );
                    $updateRes = $this->common_model->updateRecord($updateStatus, $whereCond, $table_name);

                    if (!empty($updateRes)) {
                        $this->response(array(
                            "valid" => true,
                            "status" => 'OK',
                            "result" => array(
                                "message" => "Data Updated Successfully"
                            )
                        ), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array(
                            "valid"   => false,
                            "status"  => 'NOK',
                            "result"  => array(
                                "message" => RECORD_NOT_EXIST
                            )
                        ), REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response(array(
                        "valid"   => false,
                        "status"  => 'NOK',
                        "result"  => array(
                            "message" => INVALID_METHOD
                        )
                    ), REST_Controller::HTTP_OK);
                }
            }
    }

}
