<?php
defined('BASEPATH') or exit('No direct script access allowed');

// require APPPATH . 'composer/vendor/autoload.php';
require 'vendor/autoload.php';

use Aws\S3\S3Client;

function s3_get_client()
{
    $s3     = S3Client::factory([
        "credentials"   => [
            "key"       => AWS_ACCESSKEY,
            "secret"    => AWS_SECRET_KEY,
        ],
        "region"        => "ap-south-1",
        "signature"     => 'v4',
    ]);
    return $s3;
}

if (!function_exists("upload_image_s3")) {
    function upload_image_s3($image, $mimeType = "", $file_name)
    {
        $s3         = s3_get_client();
        $result     = $s3->putObject(
            [
                'Bucket'        => AMAZONS3_BUCKET,
                'Key'           => $file_name,
                'SourceFile'    => $image['tmp_name'],
                'ContentType'   => $mimeType,
                'ACL'           => 'public-read'
            ]
        );
        return $result['ObjectURL'];
        
    }
}
if (!function_exists("upload_image_s3_grievance")) {
    
    function upload_image_s3_grievance($image, $mimeType = "")
    {
        $s3 = s3_get_client();

        $result = $s3->putObject(
            [
                'Bucket' => AMAZONS3_BUCKET,
                'Key' => "grievance/" . uniqid() . "." . $image['name'],
                'SourceFile' => $image['tmp_name'],
                'ContentType' => $mimeType,
                'ACL' => 'public-read',
                'StorageClass' => 'REDUCED_REDUNDANCY',
            ]
        );
        return $result['ObjectURL']; 
    }
}


if (!function_exists("upload_file_s3")) {
    function upload_file_s3($image, $mimeType = "", $file_name)
    {
        $s3         = s3_get_client();
        $result     = $s3->putObject(
            [
                'Bucket'        => AMAZONS3_BUCKET,
                'Key'           => $file_name,
                'Body'    => $image,
                'ACL'           => 'public-read'
            ]
        );
        return $result['ObjectURL'];
        
    }
}


if (!function_exists("s3_image_uploader")) {
    function s3_image_uploader($image, $imageName, $mimeType = "")
    {
        $s3         = s3_get_client();
        $result     = $s3->putObject(
            [
                'Bucket'        => AMAZONS3_BUCKET,
                'Key'           => $imageName,
                'SourceFile'    => $image['tmp_name'],
                'ContentType'   => $mimeType,
                'ACL'           => 'public-read',
                'StorageClass'  => 'REDUCED_REDUNDANCY',
            ]
        );

        return $result['ObjectURL'];
    }
}

if (!function_exists("s3_delete_single_image")) {
    function s3_delete_single_image($imageKey)
    {
        $s3                 = s3_get_client();
        try {
            // Delete single file
            $result         = $s3->deleteObject([
                'Bucket'    => AMAZONS3_BUCKET,
                'Key'       => $imageKey,
            ]);

        } catch (\Exception $error) {
            $result         = array();
        }
        
        return $result;
    }
}


if (!function_exists("s3_delete_image")) {
    function s3_delete_image($imageKey)
    {
        $s3                     = s3_get_client();
        $imageKeyArray          = [];
        if (is_string($imageKey)) {
            $imageKeyArray      = [
                ['Key'          => preg_replace("/^.*s3.amazonaws.com\//", "", $imageKey)],
            ];
        } elseif (is_array($imageKey)) {
            $imageKeyArray      = array_map(function ($image) {
                return ['Key'   => preg_replace("/^.*s3.amazonaws.com\//", "", $image)];
            }, $imageKey);
        } else {
            return false;
        }

        //print_r($imageKeyArray);exit;

        try {

            // Delete multiple files
            $deleteObjArray     = [
                'Bucket'        => AMAZONS3_BUCKET,
                'Delete'        => [ 
                    'Objects'   => $imageKeyArray,
                    'Quiet'     => true || false,
                ]
            ];

            $result             = $s3->deleteObjects($deleteObjArray);

        } catch (\Exception $error) {
            $result             = array();
        }
        
        return $result;
    }
}
