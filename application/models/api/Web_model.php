<?php

class Web_model extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
  }

  public function send_mail($from,$to,$subject,$message)
    {
        $this->load->library('email'); 
        $this->email->set_mailtype("html"); 
        $this->email->from($from,'Mostlikers');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);  
        $this->email->send();       
        return $this->email->print_debugger(); 
    }
}
